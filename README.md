## train.py
- [x] -train_epoch
- [x] -eval_model
- [x] -train_model
- [x] -plot_history

## prepare_data.py
- [x] parse annotations to dataframe (with tags)
- [x] split dataframe on train and test dataframe


## predict.py
- [ ] images_predict (with heatmap grad-cam)
- [x] dataloader_predict(model, data_loader, confusion_matrix=True)
- [x] plot_confusion_matrix(y_test, y_pred, class_names)
- [x] show_predictions(model, data_loader, n_images)


## dataset.py
- [x] class BagDataset
- [x] show_batch with labels

## model.py
- [x] create_model

## utils.py
_(realise using PIL)_
- [x] load_image
- [x] show_image
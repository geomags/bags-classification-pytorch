from mybagsclassifier import (prepare_data, dataset, model, train, predict)
import matplotlib.pyplot as plt
import torchvision.transforms as T
import pandas as pd
import numpy as np
import torch
from  torch.utils.data import DataLoader
from PIL import Image
from pathlib import Path
import cv2

annotation_path = Path('annotations.xml')
images_path = Path('images')

DATASETS = ('train', 'val', 'test')
CLASSES = ('scheme_M', 'scheme_W', 'empty')

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)


ann_dict = prepare_data.parse_cvat_annotations_by_tag(annotation_path)
X_train, X_val, X_test, y_train, y_val, y_test = prepare_data.train_val_test_split(ann_dict['Name'], ann_dict['Class'], parts_sizes=(0.7, 0.15, 0.15))

train_df = pd.DataFrame(list(zip(X_train, y_train)), columns=['Name', 'Class'])
# print(f'train_df consists {len(train_df)} images')
# train_df.Class.hist()
# plt.show()

val_df = pd.DataFrame(list(zip(X_val, y_val)), columns=['Name', 'Class'])
# print(f'val_df consists {len(val_df)} images')
# train_df.Class.hist()
# plt.show()

test_df = pd.DataFrame(list(zip(X_test, y_test)), columns=['Name', 'Class'])
# print(f'test_df consists {len(test_df)} images')
# train_df.Class.hist()
# plt.show()

dataset_df = {
    'train' : train_df,
    'val' : val_df,
    'test' : test_df
}

dataset_sizes = {d: len(dataset_df[d]) for d in DATASETS}
print(dataset_sizes)

# img = Image.open(images_path/train_df.iloc[10].Name)
# plt.imshow(img)
# plt.show()

# -----------------------------------------------------------------------------------------------
# -------------------------------------| Transformations |---------------------------------------
# -----------------------------------------------------------------------------------------------

mean_nums = [0.485, 0.456, 0.406]
std_nums = [0.229, 0.224, 0.225]

transforms = {'train': T.Compose([
  # Paste this
  T.ToPILImage(),
  
  T.CenterCrop(size=(400, 500)), # try this

  T.Resize(size=224),
  T.CenterCrop(size=224),

  # Instead of this
  # T.RandomResizedCrop(size=256),
  # T.RandomRotation(degrees=15),
  # T.RandomHorizontalFlip(),

  T.ToTensor(),
  T.Normalize(mean_nums, std_nums)
]), 'val': T.Compose([                      
  T.ToPILImage(),

  T.CenterCrop(size=(400, 500)), # try this

  T.Resize(size=224),
  T.CenterCrop(size=224),
  T.ToTensor(),
  T.Normalize(mean_nums, std_nums)
]), 'test': T.Compose([
  T.ToPILImage(),

  T.CenterCrop(size=(400, 500)),  # try this

  T.Resize(size=224),
  T.CenterCrop(size=224),
  T.ToTensor(),
  T.Normalize(mean_nums, std_nums)
]),
}


# -----------------------------------------------------------------------------------------------
# ---------------------------------------| Dataloaders |-----------------------------------------
# -----------------------------------------------------------------------------------------------

data_loaders = {
    d : DataLoader(dataset.BagsDataset(dataset_df[d], images_path, classes=CLASSES, 
                    transform=transforms[d]), batch_size=4, shuffle=True, 
                    num_workers=4) for d in DATASETS
}

# -----------------------------------------------------------------------------------------------
# ------------------------------------------| Model |--------------------------------------------
# -----------------------------------------------------------------------------------------------

base_model = model.create_resnet50_model(len(CLASSES), pretrained=True)

base_model, history = train.train_model(base_model, data_loaders, dataset_sizes, device, n_epochs=1)

train.plot_training_history(history)
plt.show()




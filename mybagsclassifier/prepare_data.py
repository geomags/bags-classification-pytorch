import pandas as pd
from pathlib import Path
import xml.etree.ElementTree as ET
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


def parse_cvat_annotations_by_tag(annotations_path):
    """Function for parse CVAT annotation files.
    !It works for images with a single tag!

    Args:
        annotations_path (Path or string): path to the annotation XML file

    Returns:
        dict ('Name', 'Class'): dictionary with two lists: file names and classes
    """
    
    tree = ET.parse(annotations_path)
    root = tree.getroot()

    image_names = []
    image_classes = []

    for image in root.iter('image'):
        tags = list(image.iter('tag'))

        if len(tags):
            image_names.append(image.attrib['name'])
            image_classes.append(tags[0].attrib['label'])

    annotations_dict = {
        'Name' : image_names,
        'Class' : image_classes
    }
    
    return annotations_dict


def train_val_test_split(X, y, parts_sizes=(0.7, 0.15, 0.15)):
    """Function for split dataset to Train, Validation and Test datasets

    Args:
        X (list): list of data
        y (list): list of labels
        parts_sizes (list, optional): Sizes of the parts. Should be equal 1.00. Defaults to [0.7, 0.15, 0.15].

    Returns:
        6 lists: X_train, X_val, X_test, y_train, y_val, y_test
    """

    if round(sum(parts_sizes), 1) != 1.00:
        raise ValueError(f'parts_sizes must be equal 1.00, but receiverd {sum(parts_sizes)}!')

    if len(parts_sizes) != 3:
        raise ValueError(f'parts_sizes must consist 3 sizes, but receiverd {len(parts_sizes)}!')


    X_train, X_test, y_train, y_test = train_test_split(X, 
                                                        y,
                                                        train_size=0.7, 
                                                        stratify=y)

    val_test_sum = parts_sizes[1] + parts_sizes[2]
    rel_val_size = (parts_sizes[1] * 100) / val_test_sum
    
    X_val, X_test, y_val, y_test = train_test_split(X_test, 
                                                    y_test,
                                                    train_size=rel_val_size / 100, 
                                                    stratify=y_test)

    return X_train, X_val, X_test, y_train, y_val, y_test


if __name__ == '__main__':
    annotation_path = Path('../annotations.xml')
    ann_dict = parse_cvat_annotations_by_tag(annotation_path)

    assert len(ann_dict['Name']) == len(ann_dict['Class']) != 0, 'something wrong'

    X_train, X_val, X_test, y_train, y_val, y_test = train_val_test_split(ann_dict['Name'], ann_dict['Class'], parts_sizes=(0.7, 0.15, 0.15))


    train_df = pd.DataFrame(list(zip(X_train, y_train)), columns=['Name', 'Class'])
    print(train_df)
    train_df.Class.hist()
    plt.show()

    val_df = pd.DataFrame(list(zip(X_val, y_val)), columns=['Name', 'Class'])
    print(val_df)
    train_df.Class.hist()
    plt.show()

    test_df = pd.DataFrame(list(zip(X_test, y_test)), columns=['Name', 'Class'])
    print(test_df)
    train_df.Class.hist()
    plt.show()
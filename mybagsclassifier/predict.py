from sklearn.metrics import confusion_matrix
from torch.utils.data import DataLoader
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
import torch
import time
import cv2


calc_avg = lambda x: sum(x) / len(x)

def predict_dataloader(model, device, data_loader, class_names, conf_matrix=True):
    model = model.eval()
    predictions = []
    real_values = []
    times = []

    with torch.no_grad():
        for inputs, labels in data_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            time1 = time.time()
            outputs = model(inputs)
            res_time = (time.time() - time1) * 1000
            _, preds = torch.max(outputs, 1)

            predictions.extend(preds)
            real_values.extend(labels)
            times.append(res_time)

    predictions = torch.as_tensor(predictions).cpu()
    real_values = torch.as_tensor(real_values).cpu()

    print(f'[INFO] Average prediction time is: {calc_avg(times):.3f}ms')

    plt.plot(range(len(times)), times,
            label=f'prediction time (avg. {calc_avg(times):.3f}ms)')
    plt.ylabel('Prediction time, ms')
    plt.legend()
    plt.show()

    # Show Confusion Matrix
    if conf_matrix:
        cm = confusion_matrix(real_values, predictions)

        cell_counts = cm.flatten()

        cm_row_norm = cm / cm.sum(axis=1)[:, np.newaxis]

        row_percentages = ["{0:.2f}".format(value) for value in cm_row_norm.flatten()]

        cell_labels = [f"{cnt}\n{per}" for cnt, per in zip(cell_counts, row_percentages)]
        cell_labels = np.asarray(cell_labels).reshape(cm.shape[0], cm.shape[1])

        df_cm = pd.DataFrame(cm_row_norm, index=class_names, columns=class_names)

        hmap = sns.heatmap(df_cm, annot=cell_labels, fmt="", cmap="Blues")
        hmap.yaxis.set_ticklabels(hmap.yaxis.get_ticklabels(), rotation=0, ha='right')
        hmap.xaxis.set_ticklabels(hmap.xaxis.get_ticklabels(), rotation=30, ha='right')
        plt.ylabel('True Sign')
        plt.xlabel('Predicted Sign')
        plt.show()
    
    return predictions, real_values



import torch.utils.data
import torch
import matplotlib.pyplot as plt
import numpy as np
import os.path
import random
import time
import cv2


class BagsDataset(torch.utils.data.Dataset):
    def __init__(self, dataframe, images_dir, classes=('scheme_M', 'scheme_W', 'empty'), transform=None):
        self.dataframe = dataframe
        self.transform = transform
        self.classes = classes
        self.images_dir = images_dir

    def __len__(self):
        return len(self.dataframe)

    def __getitem__(self, index):
        image_path = os.path.join(self.images_dir, self.dataframe.iloc[index].Name)
        image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
        label = self.dataframe.iloc[index].Class
        label = list(self.classes).index(label)

        if self.transform is not None:
            image = self.transform(image)

        return image, label

    def show_batch(self, n_images=8, figsize=(20, 10)):

        cols = 4
        rows = n_images // cols

        plt.figure(figsize=figsize)

        for i, sample in enumerate(self.dataframe.sample(n_images).iloc):
            img_path = os.path.join(self.images_dir, sample.Name)
            image = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
            label = sample.Class
            
            ax = plt.subplot(rows + 1, cols, i + 1)
            ax.imshow(image)
            ax.set_title(label)
            ax.axis('off')

        plt.plot()

    def show_predictions(self, model, device, n_images=12, figsize=(20, 10)):
           
        model = model.eval()
        cols = 4
        rows = n_images // cols

        plt.figure(figsize=figsize)

        inputs = []
        labels = []
        idxs = []

        for i in range(n_images):
            index = random.randint(0, self.__len__())
            idxs.append(index)
            inp, lbl = self.__getitem__(index)
            inputs.append(inp)
            labels.append(lbl)

        inputs = torch.stack(inputs)
        labels = torch.from_numpy(np.array(labels))

        with torch.no_grad():

            inputs = inputs.to(device)
            labels = labels.to(device)

            time1 = time.time()
            outputs = model(inputs)
            res_time = (time.time() - time1) * 1000
            print(f'[INFO] {n_images} batch prediction time is: {res_time:.3f}ms')
            print(f'[INFO] Average time per image is: {res_time / n_images:.3f}ms')
            _, preds = torch.max(outputs, 1)

            for i in range(n_images):

                img_path = os.path.join(self.images_dir, self.dataframe.Name.iloc[idxs[i]])
                image = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)

                ax = plt.subplot(rows + 1, cols, i + 1)
                ax.imshow(image)
                ax.set_title(f'predicted: {self.classes[int(preds[i])]}\nactual: {self.classes[int(labels[i])]}')
                ax.axis('off')

            plt.plot()
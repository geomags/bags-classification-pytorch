import torch
import torchvision.models


def create_resnet50_model(n_classes, pretrained=True, device=None):
    model = torchvision.models.resnet50(pretrained=pretrained)

    n_features = model.fc.in_features
    model.fc = torch.nn.Linear(n_features, n_classes)

    if not device:
        device = torch.device('cpu')

    return model.to(device)

if __name__ == '__main__':
    base_model = create_resnet50_model(3, False)
    print(base_model)
